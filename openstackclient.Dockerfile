FROM python:3.13-slim-bookworm@sha256:026dd417a88d0be8ed5542a05cff5979d17625151be8a1e25a994f85c87962a5

# NOTE: please keep the list of packages to install sorted for easier visual
# scanning of what is available in the image.
RUN set -eu; \
    export BUILD_PACKAGES="build-essential libssl-dev libffi-dev" ; \
    export REQUIRED_PACKAGES="bash-completion curl jq less procps psmisc screen tini tmux vim" ; \
    apt-get update; \
    apt-get install -y --no-install-recommends ${BUILD_PACKAGES} ${REQUIRED_PACKAGES} ; \
    # explicitly install setuptools: https://github.com/docker-library/python/pull/954
    pip install setuptools ; \
    pip install openstackclient ; \
    pip install osc-placement ; \
    # install python-ironic-inspector-client to get introspection
    pip install python-ironic-inspector-client ; \
    apt-get purge --auto-remove -y ${BUILD_PACKAGES} ; \
    rm -rf /var/lib/apt/lists ; \
    mkdir -p /etc/bash_completion.d/ ; \
    openstack complete | tee /etc/bash_completion.d/osc.bash_completion > /dev/null; \
    echo "source /etc/bash_completion" >> /etc/bash.bashrc ; \
    echo "alias o=openstack" >> /etc/bash.bashrc ; \
    echo "complete -F _openstack o" >> /etc/bash.bashrc

# home is where vim does *not* enable mouse support.
COPY vimrc.local /etc/vim/vimrc.local

CMD ["bash", "-l"]
